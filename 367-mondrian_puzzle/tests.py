import unittest

from mondrian.mondrian import defect, get_rectangles


class TestMondrian(unittest.TestCase):

    def test_can_calculate_defect_of_small_square(self):
        self.assertEqual(defect(4), 5)
        self.assertEqual(defect(6), 6)

    def test_can_generate_list_of_rectangles(self):
        self.assertEqual(get_rectangles(3), {(2, 2), (1, 3), (1, 2), (1, 1)})
        self.assertEqual(get_rectangles(4), {(1, 1), (1, 2), (1, 3), (1, 4),
                                             (2, 2), (2, 3), (2, 4)})


if __name__ == "__main__":
    unittest.main()
