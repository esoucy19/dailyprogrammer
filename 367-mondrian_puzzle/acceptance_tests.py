import unittest

from mondrian.mondrian import Mondrian as M


class TestMondrianPuzzle(unittest.TestCase):

    def test_challenge_input(self):
        """Target defects provided by https://oeis.org/A276523"""
        self.assertEqual(M.defect(4), 5)
        self.assertEqual(M.defect(8), 8)
        self.assertEqual(M.defect(10), 7)
        self.assertEqual(M.defect(20), 9)
        self.assertEqual(M.defect(25), 10)
        self.assertEqual(M.defect(32), 12)


if __name__ == "__main__":
    unittest.main()
