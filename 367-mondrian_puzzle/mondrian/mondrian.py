#!/usr/bin/env python3


def defect(n):
    """For a given n x n square, fill with non-congruent rectangles until the
    difference between the largest and smallest rectangles' areas is a small as
    possible. The resulting difference is known as the defect.
    We will be implementing Hannes Bassen's algorithm to solve this problem.
    int -> int"""


# Step 1: Getting rectangles
def get_rectangles(n):
    """Generate a list of all non-congruential rectangles that fit inside
    a square of size n. Rectangles with an area larger than half of the
    square area are omitted as they would increase the defect.
    int -> [(int, int)]"""
    area = n * n
    rectangles = set()
    for height in range(1, n + 1):
        for width in range(1, n + 1):
            if (height, width) in rectangles or (width, height) in rectangles:
                # Reject congruent rectangle
                continue
            elif height * width <= area / 2:
                rectangles.add((height, width))
    return rectangles
